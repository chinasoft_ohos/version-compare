package io.g00fy2.versioncompare;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

public class VersionEqualsVerifier {

  @Test
  public void equalsContract() {
    EqualsVerifier.forClass(Version.class)
        .withIgnoredFields("originalString", "subversionNumbers", "suffix")
        .withNonnullFields("subversionNumbers", "subversionNumbersWithoutTrailingZeros", "suffix")
        .verify();
  }
}