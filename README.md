# version-compare

#### 项目介绍
- 项目名称：version-compare
- 所属系列：openharmony的第三方组件适配移植
- 功能：软件版本号比较工具 
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 1.4.1

#### 效果演示
![screenshot](screenshot/version-compare.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```groovy
// 添加maven仓库  
repositories {  
  maven {   
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'  
    }  
}
```
2.在entry模块的build.gradle文件中，
```groovy
// 添加依赖库  
dependencies {  
    implementation 'com.gitee.chinasoft_ohos:version-compare:1.0.0'   
}

```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
要比较两个版本字符串，只需创建一个新的version对象。无效输入(null或非数字的第一个字符)默认将被处理为0.0.0。

boolean result;
result = new Version("1.2.1").isHigherThan("1.2"); // result = true  
result = new Version("1.0.2-rc2").isLowerThan("1.0.2-rc3"); // result = true  
result = new Version("1.3").isEqual("1.3.0"); // result = true  
result = new Version("2.0.0-beta").isAtLeast("2.0"); // result = false  
result = new Version("2.0.0-beta").isAtLeast("2.0", /* ignoreSuffix: */ true); // result = true  

**Notes:**
* 版本号之间的分隔符是.
* 后缀和预发布编号不需要特殊的分隔符1.1rc == 1.1.rc == 1.1-rc

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0
#### 版权和许可信息
 Copyright (C) 2021 Thomas Wirth
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
