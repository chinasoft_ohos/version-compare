/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.github.g00fy2.versioncomparesample;

import java.util.List;

/**
 * StringUtils
 *
 * @since 2020-04-12
 */
public class StringUtils {
    private StringUtils() {
    }

    /**
     * 转换接口
     *
     * @param <T> 入参数
     * @param <V> 返回对象
     * @since 2020-04-12
     */
    public interface Convert<T, V> {
        /**
         * 转换
         *
         * @param object 入参数
         * @return 返回对象
         */
        V convert(T object);
    }

    /**
     * 是否为空
     *
     * @param str 判断的字符串
     * @return 空返回true
     */
    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    /**
     * 是否不为空
     *
     * @param str 判断的字符串
     * @return 不为空返回true
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 拼接字符串
     *
     * @param list 待拼接的列表
     * @param separator 分割符
     * @param convert 转换接口
     * @param <T> 列表元素的类型
     * @param <V> 转换后的类型
     * @return 转换的字符串
     */
    public static <T, V> String joinToString(List<T> list, String separator, Convert<T, V> convert) {
        StringBuilder result = new StringBuilder();
        if (list != null && convert != null) {
            for (T object : list) {
                if (result.length() > 0) {
                    result.append(separator);
                }
                result.append(convert.convert(object));
            }
        }
        return result.toString();
    }

    /**
     * 转换为字符串
     *
     * @param object 带转换的对象
     * @return 返回字符串
     */
    public static String valueOf(Object object) {
        return String.valueOf(object);
    }
}
