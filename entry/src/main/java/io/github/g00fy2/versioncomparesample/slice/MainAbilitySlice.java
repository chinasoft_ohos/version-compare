/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.github.g00fy2.versioncomparesample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.element.ShapeElement;

import io.github.g00fy2.versioncompare.Version;
import com.g00fy2.versioncomparesample.ResourceTable;
import io.github.g00fy2.versioncomparesample.StringExtension;
import io.github.g00fy2.versioncomparesample.StringUtils;

import java.util.List;

/**
 * MainAbilitySlice
 *
 * @since 2020-04-10
 */
public class MainAbilitySlice extends AbilitySlice {
    private TextField tfB;
    private TextField tfA;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_compare_button_textview).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                compareVersions();
            }
        });
        tfA = (TextField) findComponentById(ResourceTable.Id_version_a_edittext);
        tfA.setFocusChangedListener(new Component.FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean isFocus) {
                if (isFocus) {
                    ShapeElement element = new ShapeElement();
                    element.setRgbColor(RgbColor.fromArgbInt(getColor(ResourceTable.Color_colorAccent)));
                    ((TextField) component).setBasement(element);
                } else {
                    ShapeElement element = new ShapeElement();
                    element.setRgbColor(RgbColor.fromArgbInt(getColor(ResourceTable.Color_color_e)));
                    ((TextField) component).setBasement(element);
                }
            }
        });
        tfB = (TextField) findComponentById(ResourceTable.Id_version_b_edittext);
        tfB.setFocusChangedListener(new Component.FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean isFocus) {
                if (isFocus) {
                    ShapeElement element = new ShapeElement();
                    element.setRgbColor(RgbColor.fromArgbInt(getColor(ResourceTable.Color_colorAccent)));
                    ((TextField) component).setBasement(element);
                } else {
                    ShapeElement element = new ShapeElement();
                    element.setRgbColor(RgbColor.fromArgbInt(getColor(ResourceTable.Color_color_e)));
                    ((TextField) component).setBasement(element);
                }
            }
        });
        tfB.setEditorActionListener(new Text.EditorActionListener() {
            @Override
            public boolean onTextEditorAction(int i) {
                compareVersions();
                return true;
            }
        });
    }

    private void compareVersions() {
        String versionStringA = ((TextField) findComponentById(ResourceTable.Id_version_a_edittext)).getText();
        String versionStringB = ((TextField) findComponentById(ResourceTable.Id_version_b_edittext)).getText();

        Checkbox cbIsHigher = (Checkbox) findComponentById(ResourceTable.Id_is_higher_checkedtextview);
        Checkbox cbIsLower = (Checkbox) findComponentById(ResourceTable.Id_is_lower_checkedtextview);
        Checkbox cbIsEqual = (Checkbox) findComponentById(ResourceTable.Id_is_equal_checkedtextview);

        if (StringUtils.isNotEmpty(versionStringA) && StringUtils.isNotEmpty(versionStringB)) {
            Version versionA = new Version(versionStringA);
            Version versionB = new Version(versionStringB);

            cbIsHigher.setChecked(versionA.isHigherThan(versionB));
            cbIsLower.setChecked(versionA.isLowerThan(versionB));
            cbIsEqual.setChecked(versionA.isEqual(versionB));

            setVersionDescriptionViews(versionA, versionB);
        } else {
            cbIsHigher.setChecked(false);
            cbIsLower.setChecked(false);
            cbIsEqual.setChecked(false);
            findComponentById(ResourceTable.Id_version_description_linearlayout).setVisibility(Component.HIDE);
        }
        hideKeyboard();
    }

    private void setVersionDescriptionViews(Version versionA, Version versionB) {
        List<Integer> subversionAnumbers = versionA.getSubversionNumbers();
        String subversionAstr = StringUtils
            .joinToString(subversionAnumbers, ".", new StringUtils.Convert<Integer, Integer>() {
                @Override
                public Integer convert(Integer integer) {
                    return integer;
                }
            });
        Text subversionAtext = (Text) findComponentById(ResourceTable.Id_subversions_a_textview);
        subversionAtext.setText(StringExtension.getInstance(subversionAstr).ifEmpty("invalid").getString());

        Text majorAtext = (Text) findComponentById(ResourceTable.Id_major_a_textview);
        majorAtext.setText(StringUtils.valueOf(versionA.getMajor()));

        Text minorAtext = (Text) findComponentById(ResourceTable.Id_minor_a_textview);
        minorAtext.setText(StringUtils.valueOf(versionA.getMinor()));

        Text patchAtext = (Text) findComponentById(ResourceTable.Id_patch_a_textview);
        patchAtext.setText(StringUtils.valueOf(versionA.getPatch()));

        Text suffixAtext = (Text) findComponentById(ResourceTable.Id_suffix_a_textview);
        suffixAtext.setText(StringUtils.valueOf(versionA.getSuffix()));
        List<Integer> subversionBnumbers = versionB.getSubversionNumbers();
        String subversionBstr = StringUtils
            .joinToString(subversionBnumbers, ".", new StringUtils.Convert<Integer, Integer>() {
                @Override
                public Integer convert(Integer integer) {
                    return integer;
                }
            });
        Text subversionBtext = (Text) findComponentById(ResourceTable.Id_subversions_b_textview);
        subversionBtext.setText(StringExtension.getInstance(subversionBstr).ifEmpty("invalid").getString());

        Text majorBtext = (Text) findComponentById(ResourceTable.Id_major_b_textview);
        majorBtext.setText(StringUtils.valueOf(versionB.getMajor()));

        Text minorBtext = (Text) findComponentById(ResourceTable.Id_minor_b_textview);
        minorBtext.setText(StringUtils.valueOf(versionB.getMinor()));

        Text patchBtext = (Text) findComponentById(ResourceTable.Id_patch_b_textview);
        patchBtext.setText(StringUtils.valueOf(versionB.getPatch()));

        Text suffixBtext = (Text) findComponentById(ResourceTable.Id_suffix_b_textview);
        suffixBtext.setText(StringUtils.valueOf(versionB.getSuffix()));

        findComponentById(ResourceTable.Id_version_description_linearlayout).setVisibility(Component.VISIBLE);
    }

    private void hideKeyboard() {
        tfA.clearFocus();
        tfB.clearFocus();
    }

    @Override
    public void onActive() {
        super.onActive();
    }
}
