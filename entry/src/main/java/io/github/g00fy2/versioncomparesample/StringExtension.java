/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.github.g00fy2.versioncomparesample;

/**
 * String 扩展类
 *
 * @since 2020-04-10
 */
public class StringExtension {
    private String string;

    private StringExtension(String string) {
        this.string = string;
        if (this.string == null) {
            this.string = "";
        }
    }

    /**
     * 构造实例
     *
     * @param string 传入的字符串
     * @return StringExtension 实例
     */
    public static StringExtension getInstance(String string) {
        return new StringExtension(string);
    }

    /**
     * 是否为空
     *
     * @param defaultValue 为空的默认值
     * @return 返回this实例
     */
    public StringExtension ifEmpty(String defaultValue) {
        string = string == null ? "null" : string.length() == 0 ? defaultValue : string;
        return this;
    }

    public String getString() {
        return string;
    }
}
